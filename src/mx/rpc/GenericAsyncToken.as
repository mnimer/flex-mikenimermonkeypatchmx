package mx.rpc
{
	import mx.messaging.messages.IMessage;
	import mx.core.mx_internal;
	use namespace mx_internal;

	dynamic public class GenericAsyncToken extends AsyncToken
	{
	    public function applyGenericFault(result:Object):void
	    {
			if (responders != null)
	    	{
	    	    for (var i:uint = 0; i < responders.length; i++)
	            {
	                var responder:IResponder = responders[i];
	                if (responder != null)
	                {
	                    responder.fault(result);
	                }
	            }
	    	}
	    }
	
	    /**
	     * @private
	     */
	    public function applyGenericResult(info:Object):void
	    {
	        setResult(info);
	
	    	if (responders != null)
	    	{
	    	    for (var i:uint = 0; i < responders.length; i++)
	            {
	                var responder:IResponder = responders[i];
	                if (responder != null)
	                {
	                    responder.result(info);
	                }
	            }
	    	}
	    }

		public function GenericAsyncToken(message:IMessage)
		{
			super(message);
		}
		
	}
}